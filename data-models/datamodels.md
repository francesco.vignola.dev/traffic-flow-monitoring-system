# TrafficFlowObserved


-  `id`: Unique identifier of the entity
   -  Attribute type: **Property**. 
   -  Required
-  `type`: NGSI Entity type. It has to be TrafficFlowObserved. One of : `TrafficFlowObserved`.
   -  Attribute type: **Property**. 
   -  Required
-  `dateObserved`: The date and time of this observation in ISO8601 UTC format. It can be represented by an specific time instant or by an ISO8601 interval. As a workaround for the lack of support of Orion Context Broker for datetime intervals, it can be used two separate attributes: `dateObservedFrom`, `dateObservedTo`. [DateTime](https://schema.org/DateTime) or an ISO8601 interval represented as [Text](https://schema.org/Text)
   -  Attribute type: **Property**. [DateTime.](https://schema.org/DateTime.)
   -  Required
-  `areaServed`: The geographic area where a service or offered item is provided
   -  Attribute type: **Property**. [Text](https://schema.org/Text)
   -  Optional
-  `averageVehicleSpeed`: Average speed of the vehicles transiting during the observation period
   -  Attribute type: **Property**. [Number](https://schema.org/Number)
   -  Optional
-  `congested`: Flags whether there was a traffic congestion during the observation period in the referred lane. The absence of this attribute means no traffic congestion
   -  Attribute type: **Property**. [Boolean.](https://schema.org/Boolean.)
   -  Optional
-  `dateCreated`: Entity's creation timestamp.
   -  Attribute type: **Property**. [DateTime](https://schema.org/DateTime)
   -  Read-Only. Automatically generated.
-  `dateModified`: Update timestamp of this entity.
   -  Attribute type: **Property**. [DateTime](https://schema.org/DateTime)
   -  Read-Only. Automatically generated.
-  `dateObservedFrom`: Observation period start date and time. See `dateObserved`
   -  Attribute type: **Property**. [Datetime.](https://schema.org/Datetime.)
   -  Optional
-  `dateObservedTo`: Observation period end date and time. See `dateObserved`
   -  Attribute type: **Property**. [Datetime.](https://schema.org/Datetime.)
   -  Optional
-  `intensity`: Total number of vehicles detected during this observation period
   -  Attribute type: **Property**. [Number.](https://schema.org/Number.)
   -  Optional
-  `location`: Geojson reference to the item. It can be Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon
   -  Attribute type: **Geoproperty**. 
   -  Optional
-  `name`: The name of this item.
   -  Attribute type: **Property**. 
   -  Optional
-  `occupancy`: Fraction of the observation time where a vehicle has been occupying the observed lane
   -  Attribute type: **Property**. [Number.](https://schema.org/Number.)
   -  Optional
-  `levelOfService`: 'Level of Service - a qualitative measure used to relate the quality of motor vehicle traffic service. It's calculated based on speed and occupancy. The speed and occupancy thresholds that determine these service levels vary depending on the measurement point.'. One of : `FluidTraffic`, `SlowTraffic`, `Retentions`, `Congestion`.
   -  Attribute type: **Property**. 
   -  Optional
-  `roadLoad`: It represents an estimate of the degree of congestion, calculated from an algorithm that uses intensity and occupancy as variable, with certain correction factors. Sets the degree of use of the road in a range from 0% (empty) to 100% (collapsed).
   -  Attribute type: **Property**. [Number.](https://schema.org/Number.)
   -  Optional
-  `saturationFlow`: Total number of vehicles in conditions of traffic flow saturation.
   -  Attribute type: **Property**. [Number.](https://schema.org/Number.)
   -  Optional



## Examples

### OK


