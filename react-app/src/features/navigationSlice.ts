import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../app/store'

interface NavigationSettingsState {
    currentPage: string,
    drawerWidth: number
}

const initialState: NavigationSettingsState = {
    currentPage: "TRAFFIC FLOW MONITORING",
    drawerWidth: 200
}

export const navigationSettingsSlice = createSlice({
    name: 'navigationSettings',
    initialState,
    reducers: {
        setCurrentPage: (state, action: PayloadAction<string>) => {
            return { 
                ...state,
                currentPage: action.payload
            }
        },
        updateDrawerWidth: (state, action: PayloadAction<number>) => {
            return { 
                ...state,
                drawerWidth: action.payload
            }
        }
    }
});

export const { setCurrentPage, updateDrawerWidth } = navigationSettingsSlice.actions;
export const selectNavigationSettings = (state: RootState) => state.roadTraffic.flows;
export default navigationSettingsSlice.reducer;