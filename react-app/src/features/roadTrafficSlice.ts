import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../app/store'

interface RoadTrafficState {
    flows: GeoJSON.FeatureCollection | undefined
}

const initialState: RoadTrafficState = {
    flows: undefined
}

export const roadTrafficSlice = createSlice({
    name: 'roadTraffic',
    initialState,
    reducers: {
        update: (state, action: PayloadAction<GeoJSON.FeatureCollection>) => {
            return { flows: action.payload }
        }
    }
});

export const { update } = roadTrafficSlice.actions;
export const selectRoadTraffic = (state: RootState) => state.roadTraffic.flows;
export default roadTrafficSlice.reducer;