import { createContext } from 'react'
import { useAppDispatch as useDispatch } from '../app/hooks'
import { update } from '../features/roadTrafficSlice';

const WebSocketContext = createContext<{ socket: WebSocket } | null>(null);

export { WebSocketContext }

type Props = {
    children: JSX.Element,
};

const WEB_SOCKET_SERVER_URL: string = 'ws://localhost:8080/map/traffic-flow';

const WebSocketProvider = ({ children }: Props) => {
    const dispatch = useDispatch();
    let reconnectOnClose = true;

    const connection = () => {
        const socket = new WebSocket(WEB_SOCKET_SERVER_URL);
        const close = socket.close;

        // Close without reconnecting;
        socket.close = () => {
            reconnectOnClose = false;
            close.call(socket);
        }

        socket.onmessage = (event: MessageEvent) => {
            if (event.data !== 'connect') {
                const payload = JSON.parse(event.data);
                dispatch(update(payload));
            }
        }

        socket.onclose = () => {      
            if (!reconnectOnClose) {
              console.log('ws closed by app');
              return;
            }
      
            console.log('ws closed by server');
      
            setTimeout(connection, 1000);
        }

        return socket;
    }

    let ws = {
        socket: connection()
    }

    return (
        <WebSocketContext.Provider value={ws}>
            { children }
        </WebSocketContext.Provider>
    );
}

export default WebSocketProvider;