import { configureStore } from '@reduxjs/toolkit';
import roadTrafficReducer from '../features/roadTrafficSlice';
import navigationReducer from '../features/navigationSlice';

const store = configureStore({
    reducer: {
      roadTraffic: roadTrafficReducer,
      navigation: navigationReducer
    },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type
export type AppDispatch = typeof store.dispatch

export default store;