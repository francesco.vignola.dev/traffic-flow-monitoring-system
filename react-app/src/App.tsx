import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';

import MyAppBar from './components/AppBar';
import MyDrawer, { DrawerHeader } from './components/drawer/MyDrawer';

import { Route, Routes } from 'react-router-dom';
import { styled } from '@mui/material/styles';

import { 
  useAppSelector as useSelector 
} from './app/hooks';
import MyMap from './components/map/MyMap';
import WebSocketProvider from './connector/WebSocket';
import ServiceTab from './components/panel/ServicePanel';
import DeviceTab from './components/panel/DevicePanel';

const RealTimeMap = () => {
  return (
    <WebSocketProvider>
      <MyMap />
    </WebSocketProvider>
  );
}

export const Main = styled('main')(({ theme }) => ({
  padding: theme.spacing(0),
}));

const App = () => {
  const drawerWidth = useSelector((state) => state.navigation.drawerWidth);

  return (
    <Box>
      <CssBaseline />
      <MyDrawer />
      <MyAppBar />
      <Main sx={{
        marginLeft: `${drawerWidth}px`,
        height: '100vh',
        width: `calc(100vw - ${drawerWidth}px)`
      }}>
          <DrawerHeader />
          <Routes>
              <Route path="/" element={<RealTimeMap />} />
              <Route path="/service-provisoning" element={<ServiceTab />} />
              <Route path="/device-provisoning" element={<DeviceTab />} />
          </Routes>
      </Main>
    </Box>
  );
}

export default App;
