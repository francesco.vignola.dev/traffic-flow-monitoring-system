import { Box } from "@mui/system";
import { DataGrid } from '@mui/x-data-grid';
import { useTheme } from '@mui/material/styles';
import { RenderCellExpand } from "./renderCellExpand";

interface CustomDataGridProps<T> {
  fields: Array<string>
  data: Array<T>
}

const CustomDataGrid = <T extends object>(props: CustomDataGridProps<T>) => {
    const theme = useTheme();
    const { fields, data } = props;

    return (
        <Box
          sx= {{
            height: 590,
            width: '100%',
            px: 2,
            pb: 3,
            [theme.breakpoints.up('md')]: {
              height: '100%',
              width: '60%',
              pt: 3,
              pl: 0,
              ml: -12
            }
          }}>
            <DataGrid
                rows={data}
                columns={mapperFields(fields)}
                pageSize={12}
                rowsPerPageOptions={[12]}
            />
        </Box>
    );
}

const mapperFields = (data: Array<string>) => data.map((key: string) => { return { field: key, headerName: key.replace("_", " ").toUpperCase(), width: 130, renderCell: RenderCellExpand } });

/*
const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'firstName', headerName: 'First name', width: 130 },
    { field: 'lastName', headerName: 'Last name', width: 130 },
    { field: 'age', headerName: 'Age', type: 'number', width: 90 },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
      valueGetter: (params: GridValueGetterParams) =>
        `${params.row.firstName || ''} ${params.row.lastName || ''}`,
    },
  ];

  const rows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
    { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
    { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
    { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
  ];
*/
  
export default CustomDataGrid;