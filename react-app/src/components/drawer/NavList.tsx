import './NavList.css';
import { List, ListItem, useTheme } from '@mui/material';
import { useState } from 'react';
import { ExpandableNavButton, NavButton } from './NavButton';

const getKey = (item: { key?: string, title: string }) => {
    return item.key || item.title.toLowerCase();
}

export interface ListProps {
    buttons: NavListProps[];
}

export interface NavListProps {
    key?: string;
    title: string;
    icon: JSX.Element;
    routing: RoutingProps | Array<{ title: string } & RoutingProps>;
}

export interface RoutingProps {
    route?: string;
    onClick?: () => void;
}

export const NavList = (props: ListProps) => {
    const theme = useTheme();
    const { buttons } = props;
    const [buttonKey, setButtonKey] = useState<string>(getKey(buttons[0]));

    const handleClick = (key: string) => {
        if (buttonKey !== key) {
            setButtonKey(key);
        }
    }

    const getColor = (item: { key?: string, title: string }) => {
        return buttonKey === getKey(item) ?
            theme.palette.primary.light : "primary";
    }
    
    return (
        <List component="nav">
            { buttons.map(button => (
                <ListItem
                    key={getKey(button)} 
                    disablePadding
                    sx={{
                        flexDirection: 'column',
                        alignItems: 'flex-start'
                    }}
                >
                    { Array.isArray(button.routing) ?
                        <ExpandableNavButton 
                            title={button.title}
                            icon={button.icon}
                            children={button.routing.map(subButton => {
                                return {
                                    id: getKey(subButton),
                                    title: subButton.title,
                                    route: subButton.route,
                                    color: getColor(subButton),
                                    onClick: (key: string) => {
                                        handleClick(key);
                                        if (subButton.onClick) {
                                            subButton.onClick();
                                        }
                                    }
                                }
                            })}
                        /> :
                        <NavButton
                            id={getKey(button)}
                            title={button.title}
                            icon={button.icon}
                            route={button.routing.route}
                            color={getColor(button)}
                            onClick={(key: string) => {
                                handleClick(key);
                                const routing = button.routing as RoutingProps;
                                if (routing.onClick) {
                                    routing.onClick();
                                }
                            }}
                        />                     
                    }
                </ListItem>
            )) }
        </List>
    );
}