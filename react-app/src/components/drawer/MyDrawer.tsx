import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import { styled } from '@mui/material/styles';

import HomeIcon from "@mui/icons-material/HomeOutlined";
import SensorsIcon from '@mui/icons-material/SensorsOutlined';

import { NavList } from './NavList';

import { 
    useAppDispatch as useDispatch, 
    useAppSelector as useSelector 
} from '../../app/hooks';
import { setCurrentPage } from '../../features/navigationSlice';

export const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const HOME_TITLE: string = "TRAFFIC FLOW MONITORING";
const DEVICE_TITLE: string = "Device Management";
const SERVICE_PROVISIONING_TITLE: string = "Service Provisioning";
const DEVICE_PROVISIONING_TITLE: string = "Device Provisioning";

const MyDrawer = () => {
    const dispatch = useDispatch();
    const drawerWidth = useSelector((state) => state.navigation.drawerWidth);

    return ( 
        <Drawer
            variant="persistent"
            anchor="left"
            open={true}
            sx={{
                display: { xs: 'none', sm: 'flex' },
                flexShrink: 0,
                width: drawerWidth,
                '& .MuiDrawer-paper': {
                    width: drawerWidth,
                    boxSizing: 'border-box',
                }
            }}
        >
            <DrawerHeader />
            <Divider />
            <NavList
                buttons={[
                    {
                        title: "Home",
                        icon: <HomeIcon />,
                        routing: {
                            route: "/",
                            onClick: () => dispatch(setCurrentPage(HOME_TITLE))
                        }
                    },
                    {
                        title: DEVICE_TITLE,
                        icon: <SensorsIcon />,
                        routing: [
                            {
                                title: SERVICE_PROVISIONING_TITLE,
                                route: "/service-provisoning",
                                onClick: () => dispatch(setCurrentPage(SERVICE_PROVISIONING_TITLE))
                            },
                            {
                                title: DEVICE_PROVISIONING_TITLE,
                                route: "/device-provisoning",
                                onClick: () => dispatch(setCurrentPage(DEVICE_PROVISIONING_TITLE))
                            }
                        ]
                    }
                ]}
            />
        </Drawer>
    );
}

export default MyDrawer;