import Collapse from '@mui/material/Collapse';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';

import { useState } from 'react';
import { Link } from 'react-router-dom';

export const NavButton = (props: NavButtonProps) => {
    const { id, title, icon, route, color, onClick } = props;

    return (
        <ListItemButton
            component={Link} to={route || `/${id}`}
            onClick={() => onClick(id)}
            sx={{ width: '100%'}}
        >
            <ListItemIcon sx={{ color: color, minWidth: '36px'}}> { icon } </ListItemIcon>
            <ListItemText sx={{ color: color }} primary={title} />
        </ListItemButton>
    );
}

interface NestedNavButtonProps {
    id: string;
    title: string;
    route?: string;
    color: string;
    onClick: (k: string) => void;
}

interface ExpandableNavButtonProps {
    icon: JSX.Element;
    title: string;
    children: Array<NestedNavButtonProps>;
} 

export const ExpandableNavButton = (props: ExpandableNavButtonProps) => {
    const { title, icon, children } = props;
    const [open, setOpen] = useState(false);

    return (
        <>
            <ListItemButton onClick={() => setOpen(!open)}>
                <ListItemIcon sx={{ minWidth: '36px'}}> { icon } </ListItemIcon>
                <ListItemText primary={title} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    {children.map(({ id, title, route, color, onClick }) => (
                        <ListItemButton 
                            component={Link} to={route || `/${id}`}
                            onClick={() => onClick(id)}
                            sx={{ pl: 4 }}
                        >
                            <ListItemText sx={{ color: color }} primary={title} />
                        </ListItemButton>
                    ))}
                </List>
            </Collapse>
      </>
    );
}

export interface NavButtonProps {
    id: string
    title: string
    icon: JSX.Element
    route?: string
    color: string
    onClick: (k: string) => void
}