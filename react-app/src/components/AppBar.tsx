import Avatar from '@mui/material/Avatar';
import Badge from '@mui/material/Badge';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Toolbar from '@mui/material/Toolbar';

import NotificationsIcon from '@mui/icons-material/Notifications';

import {  
    useAppSelector as useSelector 
} from '../app/hooks';


const MyAppBar = () => {
    const title = useSelector((state) => state.navigation.currentPage);
    const drawerWidth = useSelector((state) => state.navigation.drawerWidth);

    return (
        <MuiAppBar position="fixed" sx={{ width: `calc(100% - ${drawerWidth}px)` }}>
            <Toolbar>
                <Typography variant="h6" noWrap component="div">
                    { title.toUpperCase() }
                </Typography>
                <Box sx={{ flexGrow: 1 }} />
                <Box sx={{ display: 'flex' }}>
                    <IconButton
                        size="large"
                        aria-label="show 17 new notifications"
                        color="inherit"
                    >
                        <Badge badgeContent={17} color="error">
                            <NotificationsIcon />
                        </Badge>
                    </IconButton>
                    <IconButton
                        size="large"
                        edge="end"
                        aria-label="account of current user"
                        aria-controls="primary-search-account-menu"
                        aria-haspopup="true"
                        onClick={() => {}}
                        color="inherit"
                    >
                        <Avatar alt="Remy Sharp" src="/5260-48.jpg" />
                    </IconButton>
                </Box>
            </Toolbar>
        </MuiAppBar>
    );
}

export default MyAppBar;