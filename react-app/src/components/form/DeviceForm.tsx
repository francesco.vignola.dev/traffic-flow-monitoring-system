import { checkValidity } from '../../model/IoTDevice';
import JsonForm from './JsonForm';
import axios from 'axios';
import IoTDevice from '../../model/IoTDevice';

const URL = "http://localhost:8080/iot/devices";

interface DeviceFormProps {
    onSave: () => void
}

const DeviceForm = (props: DeviceFormProps) => {
    const { onSave } = props;

    const handleSave = (data: Array<IoTDevice>) => {
        axios.post(URL, data).then(res => {
            onSave();
            alert("Device provisioned successful!");
        })
        .catch(error => {
            alert("Device provisioned failed!");
        });
    }

    return (
        <JsonForm
            id="devices_field"
            label="Devices Configuration"
            helperText="Should be a valid JSON"
            jsonValidator={checkValidity}
            onSave={handleSave}
            startAdornment="{ devices: ["
            endAdornment="]}"
        />
    );
}

export default DeviceForm;
