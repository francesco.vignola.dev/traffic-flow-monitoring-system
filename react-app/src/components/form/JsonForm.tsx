import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { useTheme } from '@mui/material/styles';
import { useState } from 'react';
import InputAdornment from '@mui/material/InputAdornment';

interface JsonFormProps<T> {
    id: string
    label: string
    defaultValue?: string
    helperText: string
    jsonValidator: (objects: Array<T>) => boolean
    onSave: (data: Array<T>) => void
    startAdornment?: string
    endAdornment?: string
}

const JsonForm = <T extends unknown>(props: JsonFormProps<T>) => {
    const theme = useTheme();
    const [json, setJson] = useState('');
    const [error, setError] = useState(false);

    const {
        id,
        label,
        defaultValue,
        helperText,
        jsonValidator,
        onSave,
        startAdornment,
        endAdornment
    } = props;

    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.code === "Tab") {
            event.preventDefault();

            const eventElement = event.target as HTMLInputElement;
            const cursorPosition = eventElement.selectionStart;
            const cursorEndPosition = eventElement.selectionEnd;

            eventElement.value = json + "\t";
            eventElement.selectionStart = cursorPosition && cursorPosition + 1;
            eventElement.selectionEnd = cursorEndPosition && cursorEndPosition + 1;
        }
        return true;
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setJson(event.target.value);
        return true;
    }

    const handleSave = () => {
        try {
            const configuration: Array<T> = JSON.parse("[" + json + "]");
            const isValid = jsonValidator(configuration);
            setError(!isValid);
            if (isValid) {
                onSave(configuration);
            }
        } catch (error) {
            setError(true);
        }
    };

    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': {
                    width: '70%'
                },
                pl: 2,
                py: 3,
                width: '98%',
                [theme.breakpoints.up('md')]: {
                    width: '50%'
                }
            }}
            noValidate
            autoComplete="off"
        >
            <TextField
                required
                multiline
                id={id}
                label={label}
                error={error}
                helperText={error && helperText}
                defaultValue={defaultValue}
                maxRows={29}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                InputProps={{
                    startAdornment: <InputAdornment position="start">{startAdornment}</InputAdornment>,
                    endAdornment: <InputAdornment position="start">{endAdornment}</InputAdornment>
                }}        
            />
            <Button variant="contained" onClick={handleSave} sx={{ m: 1, border: 1 }}>Save</Button>
        </Box>
    );
}

export default JsonForm;