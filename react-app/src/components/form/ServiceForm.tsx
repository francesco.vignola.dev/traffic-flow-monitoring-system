import IoTService, { checkValidity } from '../../model/IoTService';
import JsonForm from './JsonForm';
import axios from 'axios';

const URL = "http://localhost:8080/iot/services";

interface ServiceFormProps {
    onSave: () => void
}

const ServiceForm = (props: ServiceFormProps) => {
    const { onSave } = props;

    const handleSave = (data: Array<IoTService>) => {
        axios.post(URL, data)
            .then(res => {
                onSave();
                alert("Service group provisioned successful!");
            })
            .catch(error => {
                alert("Service group provisioned failed!");
            });
    }

    return (
        <JsonForm
            id="service_group_field"
            label="Service Group Configuration"
            helperText="Should be a valid JSON with required attribute 'apikey'"
            jsonValidator={checkValidity}
            onSave={handleSave}
            startAdornment="{ services: ["
            endAdornment="]}"
        />
    );
}

export default ServiceForm;
