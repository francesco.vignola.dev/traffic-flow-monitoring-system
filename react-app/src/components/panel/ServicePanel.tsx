import CustomDataGrid from "../datagrid/CustomDataGrid";
import ServiceForm from "../form/ServiceForm";
import Box from '@mui/material/Box';
import axios from "axios";
import { useEffect, useState } from "react";
import { useTheme } from '@mui/material/styles';
import { IoTServiceTabular, mapInTabularFormat } from "../../model/IoTService";

const URL = "http://localhost:8080/iot/services";

const ServiceTab = () => {
    const theme = useTheme();
    const [serviceCollection, setServiceCollection] = useState(new Array<IoTServiceTabular>());

    const fetchAllServices = () => {
        axios.get(URL).then(res => {
            setServiceCollection(mapInTabularFormat(res.data));
        });
    }

    useEffect(() => {
        fetchAllServices();
//        setTimeout(fetchAllServices, 30000);
    }, []);

    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            height: '91.8vh',
            width: '100%',
            [theme.breakpoints.up('md')]: {
                flexDirection: 'row'
            }
        }}>
            <ServiceForm onSave={fetchAllServices} />
            <CustomDataGrid 
                fields={["id", "context_broker", "entity", "attributes", "service", "commands"]}
                data={serviceCollection}
            />
        </Box>
    );
}

export default ServiceTab;