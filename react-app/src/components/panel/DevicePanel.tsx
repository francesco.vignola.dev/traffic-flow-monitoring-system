import CustomDataGrid from "../datagrid/CustomDataGrid";
import DeviceForm from "../form/DeviceForm";
import Box from '@mui/material/Box';
import axios from "axios";
import { useEffect, useState } from "react";
import { useTheme } from '@mui/material/styles';
import { IoTDeviceTabular, mapInTabularFormat } from "../../model/IoTDevice";

const URL = "http://localhost:8080/iot/devices";

const DeviceTab = () => {
    const theme = useTheme();
    const [deviceCollection, setDeviceCollection] = useState(new Array<IoTDeviceTabular>());

    const fetchAllDevices = () => {
        axios.get(URL).then(res => setDeviceCollection(mapInTabularFormat(res.data)));
    }

    useEffect(() => {
        fetchAllDevices();
    }, []);

    console.log("devices: " + deviceCollection);
    
    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            height: '91.8vh',
            width: '100%',
            [theme.breakpoints.up('md')]: {
                flexDirection: 'row'
            }
        }}>
            <DeviceForm onSave={fetchAllDevices}/>
            <CustomDataGrid
                fields={["id", "device", "entity", "attributes", "service", "commands"]}
                data={ deviceCollection }
            />
        </Box>
    );
}

export default DeviceTab;