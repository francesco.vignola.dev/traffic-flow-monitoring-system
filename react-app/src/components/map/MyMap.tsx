import './MyMap.css'
import { LatLngExpression, Layer } from "leaflet";
import { useState } from "react";
import { MapContainer, GeoJSON, TileLayer } from "react-leaflet";
import { useAppSelector as useSelector } from "../../app/hooks";
import Loading from "./Loading";
import hash from "object-hash";
import Box from '@mui/material/Box';

interface TrafficProperties {
    id: string;
    dateObserved: string; 
    levelOfService: string; 
    averageVehicleSpeed: string;
    intensity: string;
    roadLoad: string;
    saturationFlow: string;
    occupancy: string;
}

type TrafficFeature = GeoJSON.Feature<GeoJSON.Geometry, TrafficProperties>

const featureStyle = (feature: TrafficFeature | undefined)  => {
    if (feature !== undefined) {
        const intensity = parseInt(feature.properties.intensity);

        const color = intensity >= 0 && intensity < 100 ? '#ffe69b' :
                        intensity >= 100 && intensity < 500 ? '#ffd557' :
                        intensity >= 500 && intensity < 1000 ? '#57ff81' :
                        intensity >= 1000 && intensity < 2000 ? '#5781ff' :
                        intensity >= 2000 && intensity < 3000 ? '#fa9a98' :
                        intensity >= 3000 && intensity < 5000 ? '#ff57d5' : '#fa1e19'
        return {
            fillColor: color,
            color: color,
            opacity: 1
        }
    }
    return {}
}

const MyMap = () => {
    const roadTrafficFlows = useSelector((state) => state.roadTraffic.flows);
    const [center, setCenter] = useState<LatLngExpression | undefined>([40.416775, -3.703790]);

    const onEachFeature = (feature: TrafficFeature, layer: Layer) => {
        const {
            id,
            dateObserved, 
            levelOfService, 
            averageVehicleSpeed,
            intensity,
            roadLoad,
            saturationFlow,
            occupancy
        } = feature.properties;
        layer.bindPopup(`
            <h5>${id}</h5>
            <ul>
                <li>Level Of Service: ${levelOfService.replace(/([A-Z])/g, ' $1').trim()}</li>
                <li>Average vehicle speed: ${averageVehicleSpeed}km/h</li>
                <li>Traffic Intensity: ${intensity} veh/h</li>
                <li>Saturation Flow: ${saturationFlow}veh/h</li>
                <li>Road Occupancy: ${parseFloat(occupancy).toFixed(2)}%</li>
                <li>Road Load: ${roadLoad} veh/km</li>
                <li>${dateObserved}</li>
            </ul>
        `);
    }

    return (
        <Box>
            <MapContainer 
                className="map"
                center={center}
                zoom={11} 
                scrollWheelZoom={false}
            >
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                { roadTrafficFlows !== undefined ? 
                    <GeoJSON key={hash(roadTrafficFlows)} 
                        data={roadTrafficFlows} 
                        style={featureStyle}
                        onEachFeature={onEachFeature} /> : <Loading /> }
            </MapContainer>
        </Box>
    );
}

export default MyMap;