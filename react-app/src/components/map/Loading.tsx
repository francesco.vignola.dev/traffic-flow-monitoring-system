import CircularProgress from '@mui/material/CircularProgress';

const Loading = () => {
    return (
        <CircularProgress 
            disableShrink 
            size={70}
            sx={{
                position: 'fixed',
                top: '50%',
                left: '55%',
                zIndex: 'tooltip'
            }}
        />
    );
}

export default Loading;