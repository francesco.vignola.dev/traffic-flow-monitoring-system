import { ExpressionLanguage, NgsiVersion } from "./IoTService";

type IoTDevice = {
    device_id: string;
    service: string;
    service_path: string;
    entity_name: string;
    entity_type: string;
    timezone?: string;
    timestamp?: boolean;
    apikey?: string;
    endpoint?: string;
    protocol?: string;
    transport?: string;
    attributes: Array<any>;
    lazy: Array<any>;
    commands: Array<any>;
    internal_attributes: Array<any>;
    static_attributes: Array<any>;
    expressionLanguage: ExpressionLanguage;
    explicitAttrs?: string;
    ngsiVersion: NgsiVersion;
}

export type IoTDeviceTabular = {
    id: string;
    device:string;
    entity: string;
    attributes: string;
    service: string;
    commands: string;
}

export const checkValidity = (devices: Array<IoTDevice>) => {
    const wrongDevices: Array<IoTDevice> = devices.filter(device => device.device_id === undefined || device.device_id.trim() === "");
    return wrongDevices.length === 0;
}

export const mapInTabularFormat = (deviceCollection: Array<IoTDevice>) => {
    const tabular: Array<IoTDeviceTabular> = deviceCollection.map<IoTDeviceTabular>(dev => {
        return {
            id: createAttribute(dev, ["device_id"]),
            device: createAttribute(dev, ["timezone", "endpoint"]),
            entity: createAttribute(dev, ["entity_name", "entity_type", "ngsiVersion", "expressionLanguage", "timestamp"]),
            attributes: createAttribute(dev, ["attributes", "lazy", "internal_attributes", "static_attributes"]),
            service: createAttribute(dev, ["apikey", "service", "service_path", "protocol", "transport"]),
            commands: createAttribute(dev, ["commands"])
        } as IoTDeviceTabular;
    });
    return tabular;
}

const createAttribute = (device: IoTDevice, attributes: Array<string>) => {
    let object = {};
    attributes.filter(property => isPresentIn(property, device))
        .map((property: string) => object = { ...object, [property]: device[property as keyof typeof device] });
    return JSON.stringify(object);
}


const isPresentIn = (property: string, object: IoTDevice) => {
    return property in object && object[property as keyof typeof object] !== undefined;
}

export default IoTDevice;