export enum ExpressionLanguage {
    legacy = "legacy",
    jexl = "jexl"
}

export enum NgsiVersion {
    v2 = "v2",
    ld = "ld"
}

type IoTService = {
    service?: string;
    subservice?: string;
    resource?: string;
    apikey: string;
    timestamp?: boolean;
    entity_type?: string;
    trust?: string;
    cbHost?: string;
    lazy: Array<any>;
    commands: Array<any>;
    attributes: Array<any>;
    static_attributes: Array<any>;
    internal_attributes: Array<any>;
    expressionLanguage?: ExpressionLanguage;
    explicitAttrs?: string;
    entityNameExp?: string;
    ngsiVersion?: NgsiVersion;
    defaultEntityNameConjunction?: string;
    autoprovision?: boolean;
}
/*
export type IoTServiceTabular = {
    id: { apikey: string };
    context_broker: { trust?: string, cbHost?: string };
    entity: { entityNameExp?: string, defaultEntityNameConjunction?: string, entity_type?: string, ngsiVersion: NgsiVersion, expressionLanguage: ExpressionLanguage, timestamp?: string };
    attributes: { explicitAttrs?: string, attributes: Array<any>, lazy: Array<any>, internal_attributes: Array<any>, static_attributes: Array<any> };
    service: { service: string, subservice: string, resource?: string, autoprovion?: boolean };
    commands: { commands: Array<any> };
}
*/

export type IoTServiceTabular = {
    id: string;
    context_broker: string;
    entity: string;
    attributes: string;
    service: string;
    commands: string;
}

export const checkValidity = (services: Array<IoTService>) => {
    const wrongServices: Array<IoTService> = services.filter(service => service.apikey === undefined || service.apikey.trim() === "");
//    console.log("services: " + services);
//    console.log("wrongServices: " + wrongServices);
    return wrongServices.length === 0;
}

export const mapInTabularFormat = (deviceCollection: Array<IoTService>) => {
    const tabular: Array<IoTServiceTabular> = deviceCollection.map(dev => {
        return {
            id: createAttribute(dev, ["apikey"]),
            context_broker: createAttribute(dev, ["trust", "cbHost"]),
            entity: createAttribute(dev, ["entityNameExp", "defaultEntityNameConjunction", "entity_type", "ngsiVersion", "expressionLanguage", "timestamp"]),
            attributes: createAttribute(dev, ["explicitAttrs", "attributes", "lazy", "internal_attributes", "static_attributes"]),
            service: createAttribute(dev, ["service", "subservice", "resource", "autoprovion"]),
            commands: createAttribute(dev, ["commands"])
        } as IoTServiceTabular;
    });
    return tabular;
}

const createAttribute = (device: IoTService, attributes: Array<string>) => {
    let object = {};
    attributes.filter(property => isPresentIn(property, device))
        .map((property: string) => object = { ...object, [property]: device[property as keyof typeof device] });
    return JSON.stringify(object);
}

const isPresentIn = (property: string, object: IoTService) => {
    return property in object && object[property as keyof typeof object] !== undefined;
}

export default IoTService;