package it.vignola.exception;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;

public class ConflictException extends ServerErrorException {
    public ConflictException(String message) {
        super(message, Response.Status.CONFLICT);
    }
}
