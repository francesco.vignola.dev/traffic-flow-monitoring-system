package it.vignola.service;

import io.quarkus.logging.Log;
import it.vignola.exception.ConflictException;
import it.vignola.model.Subscription;
import it.vignola.proxy.RegistrationProxy;
import it.vignola.util.SubscriptionFactory;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;
import org.jboss.resteasy.client.exception.ResteasyWebApplicationException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.Optional;

@ApplicationScoped
public class SubscriptionService {
    private static final Logger LOG = Logger.getLogger(SubscriptionService.class);
    private final RegistrationProxy proxy;
    private final SubscriptionFactory factory;

    @Inject
    public SubscriptionService(
            @RestClient RegistrationProxy proxy,
            SubscriptionFactory factory
    ) {
        this.proxy = proxy;
        this.factory = factory;
    }

    public String subscribe(Subscription subscription) throws ConflictException, InternalServerErrorException {
        try(Response res = proxy.subscribe(subscription)) {
            String id = res.getHeaderString("Location");
            Log.info("Subscription-id: " + id);
            return id;
        } catch (ResteasyWebApplicationException exception) {
            Optional<Object> entity = Optional.ofNullable(exception.getResponse().getEntity());
            String message = entity.orElse("").toString();
            if (exception.getResponse().getStatus() == Response.Status.CONFLICT.getStatusCode()) {
                throw new ConflictException(message);
            }
            throw new InternalServerErrorException(message);
        }
    }

    public String subscribe() throws ConflictException, InternalServerErrorException {
        Subscription subscription = factory.createSubscriptionForTrafficFlowChanges();
        LOG.info(subscription.toString());
        return subscribe(subscription);
    }

    public void unsubscribe(String subscriptionId) throws NotFoundException, InternalServerErrorException {
        try {
            proxy.unsubscribe(subscriptionId);
        } catch (ResteasyWebApplicationException exception) {
            if (exception.getResponse().getStatus() == 400) {
                throw new NotFoundException(exception.getResponse().getEntity().toString());
            } else {
                throw new InternalServerErrorException(exception.getResponse().getEntity().toString());
            }
        }
    }
}
