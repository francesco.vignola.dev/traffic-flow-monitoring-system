package it.vignola.resource;

import it.vignola.model.TrafficFlow;
import mil.nga.sf.geojson.Feature;
import mil.nga.sf.geojson.FeatureCollection;
import mil.nga.sf.geojson.FeatureConverter;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Path("/subscription/traffic-flow")
public class TrafficFlowResource {
    private final Logger logger;
    private final TrafficFlowSocket socket;

    @Inject
    public TrafficFlowResource(Logger logger, TrafficFlowSocket socket) {
        this.socket = socket;
        this.logger = logger;
    }

    @POST
    @Path("/observation")
    @Consumes(MediaType.APPLICATION_JSON)
    public void measurement(Collection<TrafficFlow> observations) {
        Collection<Feature> collectionOfFeatures = observations.stream().map(TrafficFlowResource::feature).toList();
        FeatureCollection featureCollection = new FeatureCollection(collectionOfFeatures);
        String featureCollectionContent = FeatureConverter.toStringValue(featureCollection);
        socket.broadcast(featureCollectionContent);
    }

    private static Feature feature(TrafficFlow observation) {
        Feature feature = FeatureConverter.toFeature(observation.getLocation().getGeometry());
        feature.setProperties(properties(observation));
        return feature;
    }

    private static Map<String, Object> properties(TrafficFlow observation) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("id", observation.getId());
        properties.put("dateObserved", observation.getDateObserved());
        properties.put("areaServed", observation.getAreaServed());
        properties.put("levelOfService", observation.getLevelOfService());
        properties.put("averageVehicleSpeed", observation.getAverageVehicleSpeed());
        properties.put("intensity", observation.getIntensity());
        properties.put("roadLoad", observation.getRoadLoad());
        properties.put("saturationFlow", observation.getSaturationFlow());
        properties.put("occupancy", observation.getOccupancy());
        return properties;
    }
}
