package it.vignola.resource;

import it.vignola.exception.ConflictException;
import it.vignola.service.SubscriptionService;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import javax.ws.rs.InternalServerErrorException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint(value="/map/traffic-flow")
@ApplicationScoped
public class TrafficFlowSocket {

    private static final Logger LOG = Logger.getLogger(TrafficFlowSocket.class);
    private static final Set<Session> sessions = new CopyOnWriteArraySet<>();

    private final SubscriptionService service;
    private String subscriptionId;

    @Inject
    public TrafficFlowSocket(SubscriptionService service) {
        this.service = service;
    }

    @OnOpen
    public void onOpen(Session session) {
        if (sessions.isEmpty() || this.subscriptionId == null) {
            try {
                this.subscriptionId = service.subscribe();
                if (this.subscriptionId != null) {
                    LOG.info("First socket connection opened!");
                    session.getAsyncRemote().sendText("connect");
                    sessions.add(session);
                }
            } catch (ConflictException | InternalServerErrorException e) {
                session.getAsyncRemote().sendText("CONNECTION FAILED");
                LOG.error("Conflict or Internal Server error on subscription request");
            }
        } else {
            session.getAsyncRemote().sendText("connect");
            sessions.add(session);
            LOG.info("New socket connection opened!");
        }
    }

    @OnClose
    public void onClose(Session session) {
        LOG.info("Socket session closed");
        this.close(session);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        LOG.info("Socket session closed on error: " + error.getMessage());
        this.close(session);
    }

    public void broadcast(String message) {
        sessions.forEach(s -> {
            s.getAsyncRemote().sendObject(message, result ->  {
                if (result.getException() != null) {
                    LOG.error("Unable to send observation: " + result.getException());
                } else {
                    LOG.info("WebSocket Server observation sent");
                }
            });
        });
    }

    private void close(Session session) {
        sessions.remove(session);
        if (sessions.isEmpty() && this.subscriptionId != null) {
            service.unsubscribe(this.subscriptionId);
        }
    }

}
