package it.vignola.resource;

import it.vignola.exception.ConflictException;
import it.vignola.model.Subscription;
import it.vignola.proxy.RegistrationProxy;
import it.vignola.service.SubscriptionService;
import it.vignola.validation.SubscriptionValidator;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/registration")
public class RegistrationResource {

    @RestClient
    RegistrationProxy proxy;

    @Inject
    SubscriptionService service;

    @POST
    @Path("/subscription")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscription(Subscription jsonSubscription) {
        if (!SubscriptionValidator.isValid(jsonSubscription)) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Entities and url attributes are mandatory").build();
        }
        try {
            String subscriptionId = this.service.subscribe(jsonSubscription);
            return Response.ok().header("subscription-id", subscriptionId).build();
        } catch (ConflictException e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        } catch (InternalServerErrorException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/subscription/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSubscription(@PathParam("id") String subscriptionId) {
        try {
            this.service.unsubscribe(subscriptionId);
            return Response.ok().build();
        } catch (NotFoundException exception) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        } catch (InternalServerErrorException exception) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(exception.getMessage()).build();
        }
    }
}
