package it.vignola.util;

import it.vignola.model.Subscription;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class SubscriptionFactory {

    @ConfigProperty(name = "smartVertical.subscriptions.endpoint")
    String URL;

    public Subscription createSubscriptionForTrafficFlowChanges() {
        Map<String, String> entity = new HashMap<>();
        entity.put("idPattern", ".*");
        entity.put("type", "TrafficFlowObserved");
        List<Map<String, String>> entities = new ArrayList<>();
        entities.add(entity);
        return Subscription.builder()
                .description("Traffic Flow Updates")
                .entities(entities)
                .url(URL + "traffic-flow/observation")
                .build();
    }
}
