package it.vignola.validation;

import it.vignola.model.Subscription;

import javax.inject.Singleton;

@Singleton
public class SubscriptionValidator {
    public static boolean isValid(Subscription subscription) {
        return subscription.getEntities() != null && !subscription.getEntities().isEmpty() &&
                subscription.getUrl() != null && !subscription.getUrl().isEmpty();
    }
}
