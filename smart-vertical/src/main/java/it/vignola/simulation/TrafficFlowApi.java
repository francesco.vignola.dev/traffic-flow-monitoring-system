package it.vignola.simulation;

import it.vignola.model.TrafficFlow;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/subscription/traffic-flow")
@RegisterRestClient(baseUri = "http://localhost:8080")
public interface TrafficFlowApi {
    @POST
    @Path("/observation")
    @Consumes(MediaType.APPLICATION_JSON)
    void post(Collection<TrafficFlow> observation);
}
