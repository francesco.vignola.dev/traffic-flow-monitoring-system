package it.vignola.simulation;

import io.quarkus.scheduler.Scheduled;
import it.vignola.model.TrafficFlow;
import mil.nga.sf.geojson.LineString;
import mil.nga.sf.geojson.Point;
import mil.nga.sf.geojson.Position;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@ApplicationScoped
public class TrafficFlowGenerator {
    private static final Logger LOG = Logger.getLogger(TrafficFlowGenerator.class);

    @RestClient
    TrafficFlowApi api;
    private final AtomicInteger counter = new AtomicInteger();
/*
    @SneakyThrows
    void sendObservation() {
        TrafficFlow observation = generateTrafficFlowObservation();
        ObjectMapper jsonMapper = new ObjectMapper();
        LOG.debug("Random Traffic Flow Generated: " + jsonMapper.writeValueAsString(observation));
        api.post(observation);
    }
*/

    @Scheduled(every="30s")
    void sendMoreObservations() {
        api.post(IntStream.range(1, ThreadLocalRandom.current().nextInt(1, 4500))
                .mapToObj(number -> generateTrafficFlowObservation())
                .collect(Collectors.toList()));
    }

    private TrafficFlow generateTrafficFlowObservation() {
//        40.532202, -3.503277                  40.53322183626246, -3.744008638731502
//        40.36477666726557, -3.74750656130408  40.389963664897365, -3.5466245594182704
        LocalDateTime dateObservedFrom = LocalDateTime.now();
        LocalDateTime dateObservedTo = dateObservedFrom.plusMinutes(5);
        return TrafficFlow.builder()
                .areaServed("Madrid")
                .dateObserved(dateObservedFrom + "/" + dateObservedTo)
                .saturationFlow(ThreadLocalRandom.current().nextInt(3000))
                .id("urn:ngsi-ld:TrafficFlowObserved:" + counter.incrementAndGet())
                .intensity(ThreadLocalRandom.current().nextInt(2000))
                .levelOfService("FluidTraffic")
                .occupancy(ThreadLocalRandom.current().nextDouble())
                .roadLoad(ThreadLocalRandom.current().nextInt(150))
                .averageVehicleSpeed(ThreadLocalRandom.current().nextInt(90))
                .location(randomLine())
                .build();
    }

    private static Point point(Double x, Double y) {
        return new Point(new Position(x, y));
    }

    private static Point randomPoint() {
        return point(
                ThreadLocalRandom.current().nextDouble(-3.74750, -3.50327),
                ThreadLocalRandom.current().nextDouble(40.36477, 40.53322)
        );
    }

    private static LineString line(Point...points) {
        return new LineString(Arrays.stream(points).toList());
    }

    private static LineString randomLine() {
        return line(randomPoint(), randomPoint());
    }

/*
    private static Point point(Double ...coordinates) {
        return Point.builder().coordinates(List.of(coordinates)).build();
    }

    private static Point randomPoint() {
        return point(
            ThreadLocalRandom.current().nextDouble(40.36477, 40.53322),
            ThreadLocalRandom.current().nextDouble(-3.74750, -3.50327)
        );
    }

    private static LineString line(Point ...points) {
        return LineString.builder().points(List.of(points)).build();
    }

    private static LineString randomLine() {
        return line(randomPoint(), randomPoint());
    }
*/
}
