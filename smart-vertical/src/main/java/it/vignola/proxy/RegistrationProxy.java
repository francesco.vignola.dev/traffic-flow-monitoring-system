package it.vignola.proxy;

import it.vignola.model.Subscription;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v2")
@RegisterRestClient
public interface RegistrationProxy {
    @POST
    @Path("/subscriptions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response subscribe(Subscription subscription);

    @DELETE
    @Path("/subscriptions/{subscription-id}")
    Response unsubscribe(@PathParam("subscription-id") String subscriptionId);
}
