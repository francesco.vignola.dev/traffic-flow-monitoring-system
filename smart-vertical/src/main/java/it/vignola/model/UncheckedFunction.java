package it.vignola.model;

@FunctionalInterface
public interface UncheckedFunction<T> {
    void apply(T t) throws Exception;
}
