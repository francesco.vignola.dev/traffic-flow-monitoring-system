package it.vignola.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;
import java.util.Map;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(using = SubscriptionSerializer.class)
public class Subscription {
    private String description;
    private Collection<Map<String, String>> entities;
    private Collection<String> attributes;
    private Map<String, String> expressions;
    private String url;
    private String expirationDate;
}
