package it.vignola.model;

import lombok.SneakyThrows;

import java.util.stream.Stream;

public class SafeCollection {
    public static <T> void forEachUnchecked(Stream<T> collection, UncheckedFunction<T> function) {
        collection.forEach(t -> safeApply(t, function));
    }

    @SneakyThrows
    private static <T> void safeApply(T t, UncheckedFunction<T> function) {
        function.apply(t);
    }
}
