package it.vignola.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Map;

public class SubscriptionSerializer extends StdSerializer<Subscription> {

    public SubscriptionSerializer() {
        this(null);
    }

    public SubscriptionSerializer(Class<Subscription> t) {
        super(t);
    }

    @Override
    public void serialize(
            Subscription subscription,
            JsonGenerator jgen,
            SerializerProvider provider
    ) throws IOException {
        jgen.writeStartObject();
            jgen.writeStringField("description", subscription.getDescription());
            jgen.writeObjectFieldStart("subject");
                jgen.writeArrayFieldStart("entities");
                    SafeCollection.forEachUnchecked(subscription.getEntities().stream(), eu -> writeEntityArray(jgen, eu));
                jgen.writeEndArray();
                if((subscription.getAttributes() != null && !subscription.getAttributes().isEmpty()) ||
                        (subscription.getExpressions() != null && !subscription.getExpressions().isEmpty())) {
                    jgen.writeObjectFieldStart("condition");
                    if (subscription.getAttributes() != null && !subscription.getAttributes().isEmpty()) {
                        jgen.writeArrayFieldStart("attrs");
                        SafeCollection.forEachUnchecked(subscription.getAttributes().stream(), jgen::writeString);
                        jgen.writeEndArray();
                    }
                    if (subscription.getExpressions() != null && !subscription.getExpressions().isEmpty()) {
                        jgen.writeObjectFieldStart("expression");
                        SafeCollection.forEachUnchecked(subscription.getExpressions().entrySet().stream(), entry -> jgen.writeStringField(entry.getKey(), entry.getValue()));
                        jgen.writeEndObject();
                    }
                    jgen.writeEndObject();
                }
            jgen.writeEndObject();
            jgen.writeObjectFieldStart("notification");
                jgen.writeObjectFieldStart("http");
                    jgen.writeStringField("url", subscription.getUrl());
                jgen.writeEndObject();
                jgen.writeStringField("attrFormat", "keyValues");
            jgen.writeEndObject();
        jgen.writeEndObject();
    }

    private static void writeEntityArray(JsonGenerator jgen, Map<String, String> entities) throws IOException {
        jgen.writeStartObject();
        SafeCollection.forEachUnchecked(entities.entrySet().stream(), entry -> jgen.writeStringField(entry.getKey(), entry.getValue()));
        jgen.writeEndObject();
    }
}
