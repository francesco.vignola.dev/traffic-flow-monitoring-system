package it.vignola.model;

import lombok.Builder;
import lombok.Data;
import mil.nga.sf.geojson.Geometry;

@Data
@Builder
public class TrafficFlow {
    private String id,
        dateObserved,
        areaServed,
        levelOfService;
    private Integer averageVehicleSpeed,
        intensity,
        roadLoad,
        saturationFlow;
    private Double occupancy;
    private Geometry location;
}
