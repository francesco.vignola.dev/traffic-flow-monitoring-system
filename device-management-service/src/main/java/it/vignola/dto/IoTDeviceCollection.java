package it.vignola.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Collection;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IoTDeviceCollection {
    private Integer count;
    private Collection<IoTDevice> devices;
}
