package it.vignola.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Collection;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IoTServiceCollection {
    private Integer count;
    private Collection<IoTService> services;
}
