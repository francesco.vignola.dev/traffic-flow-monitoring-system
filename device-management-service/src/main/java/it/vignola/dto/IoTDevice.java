package it.vignola.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Collection;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IoTDevice {
    private String device_id;
    private String service;
    private String service_path;
    private String entity_name;
    private String entity_type;
    private String timezone;
    private Boolean timestamp;
    private String apikey;
    private String endpoint;
    private String protocol;
    private String transport;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> lazy;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> attributes;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> static_attributes;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> internal_attributes;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> commands;
    private String expressionLanguage;
    private String explicitAttrs;
    private String ngsiVersion;
    private Boolean polling;
}
