package it.vignola.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IoTService {
    private String _id;
    private String service;
    private String subservice;
    private String resource;
    private String apikey;
    private Boolean timestamp;
    private String entity_type;
    private String trust;
    private String cbHost;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> lazy;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> attributes;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> static_attributes;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> internal_attributes;
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Collection<String> commands;
    private String expressionLanguage;
    private String explicitAttrs;
    private String enitityNameExp;
    private String ngsiVersion;
    private String defaultEntityNameConjunction;
    private Boolean autoprovision;

}
