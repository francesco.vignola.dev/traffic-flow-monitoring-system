package it.vignola.resource;

import it.vignola.dto.IoTDevice;
import it.vignola.dto.IoTDeviceCollection;
import it.vignola.dto.IoTService;
import it.vignola.proxy.IoTServiceProxy;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("/iot/devices")
public class IoTDeviceResource {
    @RestClient
    IoTServiceProxy proxy;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() {
        IoTDeviceCollection deviceCollection = proxy.getDevices();
        return Response.ok(deviceCollection.getDevices()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(Collection<IoTDevice> devices) {
        IoTDeviceCollection coll = new IoTDeviceCollection();
        coll.setDevices(devices);
        return proxy.postDevices(coll);
    }
}
