package it.vignola.resource;

import it.vignola.dto.IoTService;
import it.vignola.dto.IoTServiceCollection;
import it.vignola.proxy.IoTServiceProxy;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("/iot/services")
public class IoTServiceResource {

    @RestClient
    IoTServiceProxy proxy;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() {
        IoTServiceCollection serviceCollection = proxy.getServices();
        return Response.ok(serviceCollection.getServices()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(Collection<IoTService> services) {
        IoTServiceCollection coll = new IoTServiceCollection();
        coll.setServices(services);
        System.out.println(coll);
        return proxy.postServices(coll);
    }
}
