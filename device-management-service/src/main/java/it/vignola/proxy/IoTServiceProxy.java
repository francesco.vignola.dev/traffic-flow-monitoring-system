package it.vignola.proxy;

import it.vignola.dto.IoTDeviceCollection;
import it.vignola.dto.IoTServiceCollection;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/iot")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient
@ClientHeaderParam(name="fiware-service", value="openiot")
@ClientHeaderParam(name="fiware-servicepath", value="/")
public interface IoTServiceProxy {

    @GET
    @Path("/services")
    IoTServiceCollection getServices();

    @POST
    @Path("/services")
    Response postServices(IoTServiceCollection services);

    @GET
    @Path("/devices")
    IoTDeviceCollection getDevices();

    @POST
    @Path("/devices")
    Response postDevices(IoTDeviceCollection devices);
}
