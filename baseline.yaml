openapi: 3.0.3
info:
  title: Baseline Models
  description: |-
    Base Model Definitions from Smart Data Models
  version: 0.0.1

components:
  schemas:
    TrafficFlowObserved:
      description: 'An observation of traffic flow conditions at a certain place and time.'
      properties:
        id:
          anyOf:
            - description: 'Property. Identifier format of any NGSI entity'
              maxLength: 256
              minLength: 1
              pattern: ^[\w\-\.\{\}\$\+\*\[\]`|~^@!,:\\]+$
              type: string
            - description: 'Property. Identifier format of any NGSI entity'
              format: uri
              type: string
          description: 'Unique identifier of the entity'
          x-ngsi:
            type: Property
        type:
          description: 'NGSI Entity type. It has to be TrafficFlowObserved'
          enum:
            - TrafficFlowObserved
          type: string
          x-ngsi:
            type: Property
        dateObserved:
          description: 'The date and time of this observation in ISO8601 UTC format. It can be represented by an specific time instant or by an ISO8601 interval. As a workaround for the lack of support of Orion Context Broker for datetime intervals, it can be used two separate attributes: `dateObservedFrom`, `dateObservedTo`. [DateTime](https://schema.org/DateTime) or an ISO8601 interval represented as [Text](https://schema.org/Text)'
          type: string
          x-ngsi:
            model: https://schema.org/DateTime.
            type: Property
        areaServed:
          description: 'The geographic area where a service or offered item is provided'
          type: string
          x-ngsi:
            model: https://schema.org/Text
            type: Property
        averageVehicleSpeed:
          description: 'Average speed of the vehicles transiting during the observation period'
          minimum: 0
          type: number
          x-ngsi:
            model: https://schema.org/Number
            type: Property
            units: 'Kilometer per hour (Km/h)'
        congested:
          description: ' Flags whether there was a traffic congestion during the observation period in the referred lane. The absence of this attribute means no traffic congestion'
          type: boolean
          x-ngsi:
            model: https://schema.org/Boolean.
            type: Property
        dateCreated:
          description: "Entity's creation timestamp.\n"
          format: date-time
          readOnly: true
          type: string
          x-ngsi:
            type: Property
            auto-gen: true
            model: "https://schema.org/DateTime"
        dateModified:
          description: "Update timestamp of this entity.\n"
          format: date-time
          readOnly: true
          type: string
          x-ngsi: 
            type: Property
            auto-gen: true
            model: "https://schema.org/DateTime"
        dateObservedFrom:
          description: 'Observation period start date and time. See `dateObserved`'
          format: date-time
          type: string
          x-ngsi:
            model: https://schema.org/Datetime.
            type: Property
        dateObservedTo:
          description: 'Observation period end date and time. See `dateObserved`'
          format: date-time
          type: string
          x-ngsi:
            model: https://schema.org/Datetime.
            type: Property
        intensity:
          description: 'Total number of vehicles detected during this observation period'
          minimum: 0
          type: number
          x-ngsi:
            model: https://schema.org/Number.
            type: Property
        location:
          description: 'Geojson reference to the item. It can be Point, LineString, Polygon, MultiPoint, MultiLineString or MultiPolygon'
          oneOf:
            - description: 'Geoproperty. Geojson reference to the item. Point'
              properties:
                bbox:
                  items:
                    type: number
                  minItems: 4
                  type: array
                coordinates:
                  items:
                    type: number
                  minItems: 2
                  type: array
                type:
                  enum:
                    - Point
                  type: string
              required:
                - type
                - coordinates
              title: 'GeoJSON Point'
              type: object
            - description: 'Geoproperty. Geojson reference to the item. LineString'
              properties:
                bbox:
                  items:
                    type: number
                  minItems: 4
                  type: array
                coordinates:
                  items:
                    items:
                      type: number
                    minItems: 2
                    type: array
                  minItems: 2
                  type: array
                type:
                  enum:
                    - LineString
                  type: string
              required:
                - type
                - coordinates
              title: 'GeoJSON LineString'
              type: object
            - description: 'Geoproperty. Geojson reference to the item. Polygon'
              properties:
                bbox:
                  items:
                    type: number
                  minItems: 4
                  type: array
                coordinates:
                  items:
                    items:
                      items:
                        type: number
                      minItems: 2
                      type: array
                    minItems: 4
                    type: array
                  type: array
                type:
                  enum:
                    - Polygon
                  type: string
              required:
                - type
                - coordinates
              title: 'GeoJSON Polygon'
              type: object
            - description: 'Geoproperty. Geojson reference to the item. MultiPoint'
              properties:
                bbox:
                  items:
                    type: number
                  minItems: 4
                  type: array
                coordinates:
                  items:
                    items:
                      type: number
                    minItems: 2
                    type: array
                  type: array
                type:
                  enum:
                    - MultiPoint
                  type: string
              required:
                - type
                - coordinates
              title: 'GeoJSON MultiPoint'
              type: object
            - description: 'Geoproperty. Geojson reference to the item. MultiLineString'
              properties:
                bbox:
                  items:
                    type: number
                  minItems: 4
                  type: array
                coordinates:
                  items:
                    items:
                      items:
                        type: number
                      minItems: 2
                      type: array
                    minItems: 2
                    type: array
                  type: array
                type:
                  enum:
                    - MultiLineString
                  type: string
              required:
                - type
                - coordinates
              title: 'GeoJSON MultiLineString'
              type: object
            - description: 'Geoproperty. Geojson reference to the item. MultiLineString'
              properties:
                bbox:
                  items:
                    type: number
                  minItems: 4
                  type: array
                coordinates:
                  items:
                    items:
                      items:
                        items:
                          type: number
                        minItems: 2
                        type: array
                      minItems: 4
                      type: array
                    type: array
                  type: array
                type:
                  enum:
                    - MultiPolygon
                  type: string
              required:
                - type
                - coordinates
              title: 'GeoJSON MultiPolygon'
              type: object
          x-ngsi:
            type: Geoproperty
        name:
          description: 'The name of this item.'
          type: string
          x-ngsi:
            type: Property
        occupancy:
          description: 'Fraction of the observation time where a vehicle has been occupying the observed lane'
          maximum: 1
          minimum: 0
          type: number
          x-ngsi:
            model: https://schema.org/Number.
            type: Property
        
      required:
        - id
        - type
        - dateObserved
      type: object


# These paths are merely representative.
paths: 
  /ngsi-ld/v1/entities: 
    get: 
      responses: 
        ? "200"
        : 
          description:  OK
          content: 
            application/ld+json: 
              schema: 
                type: object        
