package it.vignola;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.scheduler.Scheduled;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import lombok.SneakyThrows;
import org.eclipse.microprofile.reactive.messaging.*;
import io.smallrye.reactive.messaging.annotations.Broadcast;
import io.smallrye.reactive.messaging.mqtt.MqttMessage;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntSupplier;
import java.util.stream.Stream;

import mil.nga.sf.geojson.LineString;
import mil.nga.sf.geojson.Point;
import mil.nga.sf.geojson.Position;
import org.jboss.logging.Logger;

@ApplicationScoped
public class MqttProducer {
    private static final Logger logger = Logger.getLogger(MqttProducer.class);
    private static final ObjectMapper jsonMapper = new ObjectMapper();

    private static final String topicTemplate = "/json/%s/%s/attrs";

    private static final int MINUTES_IN_SECONDS = 60; // 2 minutes to send all data (in parallel)
    private static final int MAX_CAPACITY = 4600;

    private final AtomicInteger sentObs = new AtomicInteger(0);
    private final AtomicInteger emitObs = new AtomicInteger(0);

    private final String apiKey;
    private final String openDataUrl;
    private final Emitter<String> emitter;
    private final LinkedBlockingQueue<TrafficFlow> messageQueue;

    public MqttProducer(
            @ConfigProperty(name="service.group.apiKey") String apiKey,
            @ConfigProperty(name="service.data.url") String openDataUrl,
            @Broadcast @Channel("traffic-flow") Emitter<String> emitter
    ) {
        this.apiKey = apiKey;
        this.openDataUrl = openDataUrl;
        this.emitter = emitter;
        this.messageQueue = new LinkedBlockingQueue<>(MAX_CAPACITY);
    }

    @Scheduled(every = "5m", concurrentExecution = Scheduled.ConcurrentExecution.SKIP)
    public void generateFromStream() {
        TrafficFlowSource source = new TrafficFlowSource();
        Flowable<Message<String>> flowable = source.stream(openDataUrl).map(this::createMessageWithNack);
        createAndSubscribeToFlow(flowable);
    }
/*
    @Scheduled(every = "10s")
    public void readFromQueue() {
        if(!messageQueue.isEmpty()) {
            Stream<TrafficFlow> messages = messageQueue.stream().limit(10);
            Flowable<Message<String>> flowable = Flowable.fromStream(messages).map(this::createMessageWithNack);
            createAndSubscribeToFlow(flowable);
            //messageQueue.clear();
            int count = messageQueue.size();
            logger.debug("Queued obs at this moment: " + count);
            logger.debug("Total obs at this moment: " + (count + sentObs.get()));
            //messageQueue.forEach(emitter::send);
        }
    }
*/
    private void createAndSubscribeToFlow(Flowable<Message<String>> source) {
        //Flowable<Message<String>> source1 = source.concatMap(m -> Flowable.timer(1, TimeUnit.SECONDS).map(l -> m));
        Flowable<Flowable<Message<String>>> faultToleranceFlowable = flowWithRetry(source);
        subscribe(faultToleranceFlowable);
    }

    private @NonNull Flowable<Flowable<Message<String>>> flowWithRetry(Flowable<Message<String>> source) {
        IntSupplier s = () -> ThreadLocalRandom.current().nextInt(0, MINUTES_IN_SECONDS);
        return source.window(1)
                .map(n -> n.delay(s.getAsInt(), TimeUnit.SECONDS))
                .retryWhen(this::errorHandling);
    }

    @SneakyThrows
    private void subscribe(Flowable<Flowable<Message<String>>> source) {
        final Consumer<Throwable> onError = throwable -> logger.error(throwable.getMessage());
        final Function<String, Action> onComplete = s -> () -> logger.debug(s);
        source.subscribe(
                fl -> fl.subscribe(m -> {
                    emitter.send(m);
                    int count = emitObs.incrementAndGet();
                    logger.debug("Elements emitted at this moment: " + count);
                }, onError, onComplete.apply("Sub Flow: onComplete")),
                onError,
                onComplete.apply("Main Flow: onComplete"));
    }

    private @NonNull Flowable<Long> errorHandling(Flowable<Throwable> errors) {
        return errors.map(error -> {
                    logger.error(error.getMessage());
                    return 1;
                })
                .scan(Math::addExact)
                .doOnNext(errorCount -> logger.error("No. of errors: " + errorCount))
                .delay(5, TimeUnit.SECONDS)
                .takeWhile(errorCount -> errorCount < 3)
                .flatMapSingle(errorCount -> Single.timer(errorCount, TimeUnit.SECONDS));
    }

    private Message<String> createMessageWithNack(TrafficFlow obs) {
        Message<String> message = createMessage(obs);
        return message.withAck(() -> {
                    int count = sentObs.incrementAndGet();
                    messageQueue.removeIf(o -> o.getId().equals(obs.getId()) && o.getDateObservedFrom() == obs.getDateObservedFrom());
                    int count1 = messageQueue.size();
                    logger.debug("Sent obs at this moment: " + count);
                    logger.debug("Queued obs at this moment: " + count1);
                    logger.debug("Total obs at this moment: " + (count + count1));
                    return CompletableFuture.completedFuture(null);
                })
                .withNack(throwable -> {
                    logger.error(throwable.getMessage());
                    boolean result = messageQueue.offer(obs);
                    if (!result) {
                        // the queue is full, so re-emit immediately
                        emitter.send(message);
                        logger.debug("Element re-emit immediately");
                    }
                    int count = messageQueue.size();
                    logger.debug("Queued obs at this moment: " + count);
                    logger.debug("Total obs at this moment: " + (count + sentObs.get()));
                    return CompletableFuture.completedFuture(null);
        });
    }

    private Message<String> createMessage(TrafficFlow obs) {
        String topic = createTopic(obs.getId());
        String payload = convertIntoJson(obs);
        return MqttMessage.of(topic, payload);
    }

    private String createTopic(String stationId) {
        return String.format(topicTemplate, apiKey, stationId);
    }

    private static String convertIntoJson(TrafficFlow observation) {
        String json = "";
        try {
            json = jsonMapper.writeValueAsString(observation);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }
        return json;
    }

    private static LineString randomLine() {
        return line(randomPoint(), randomPoint());
    }

    private static LineString line(Point...points) {
        return new LineString(Arrays.stream(points).toList());
    }

    private static Point randomPoint() {
        return point(
                ThreadLocalRandom.current().nextDouble(-3.74750, -3.50327),
                ThreadLocalRandom.current().nextDouble(40.36477, 40.53322)
        );
    }

    private static Point point(Double x, Double y) {
        return new Point(new Position(x, y));
    }
}