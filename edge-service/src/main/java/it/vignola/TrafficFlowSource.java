package it.vignola;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.reactivex.rxjava3.core.Emitter;
import io.reactivex.rxjava3.core.Flowable;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

public class TrafficFlowSource {
    private static final String DATE_PATTERN = "dd/MM/yyyy HH:mm:ss";
    private static final String DATE_TAG = "fecha_hora";
    private static final String OBSERVATION_TAG = "pm";
    private final XmlMapper xmlMapper = new XmlMapper();
    private Long observationDate;

    public Flowable<TrafficFlow> stream(String url) {
        return Flowable.generate(
                () -> this.staxReader(url),
                this::pushNextTrafficFlow,
                XMLStreamReader::close);
    }

    private XMLStreamReader staxReader(String url) throws XMLStreamException, IOException {
        final URL openDataMadrid = new URL(url);
        final InputStream inputStream = new BufferedInputStream(openDataMadrid.openStream());
        return XMLInputFactory.newInstance().createXMLStreamReader(inputStream);
    }

    private void pushNextTrafficFlow(XMLStreamReader reader, Emitter<TrafficFlow> emitter) throws XMLStreamException, IOException, ParseException {
        final Optional<TrafficFlow> trafficFlow = nextObservation(reader);
        trafficFlow.ifPresentOrElse(emitter::onNext, emitter::onComplete);
    }

    private Optional<TrafficFlow> nextObservation(XMLStreamReader reader) throws XMLStreamException, IOException, ParseException, IllegalStateException {
        while(reader.hasNext()) {
            if (reader.next() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equals(DATE_TAG)) {
                    long newDate = parseDate(reader);
                    if (observationDate != null && newDate == observationDate) {
                        throw new IllegalStateException("Data not updated!");
                    }
                    observationDate = newDate;
                } else if (reader.getLocalName().equals(OBSERVATION_TAG)) {
                    TrafficFlow tf = parseObservation(reader);
                    tf.setDateObservedFrom(observationDate);
                    return Optional.of(tf);
                }
            }
        }
        return Optional.empty();
    }

    private long parseDate(XMLStreamReader reader) throws IOException, ParseException {
        String dateObservationFrom = xmlMapper.readValue(reader, String.class);
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
        return dateFormatter.parse(dateObservationFrom).getTime();
    }

    private TrafficFlow parseObservation(XMLStreamReader reader) throws IOException {
        return xmlMapper.readValue(reader, TrafficFlow.class);
    }
}
