package it.vignola;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import mil.nga.sf.geojson.Geometry;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "pm")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "id",
        "name",
        "levelOfService",
        "dateObservedFrom",
        "averageVehicleSpeed",
        "intensity",
        "roadLoad",
        "saturationFlow",
        "occupancy",
        "location"
})
public class TrafficFlow {
    @JsonProperty("idelem")
    private String id;
    @JsonProperty("descripcion")
    private String name;
    @JsonProperty("nivelServicio")
    private Integer levelOfService;

    @JsonProperty("fecha_hora")
    private long dateObservedFrom;

    @JsonProperty("velocidad")
    private Integer averageVehicleSpeed;
    @JsonProperty("intensidad")
    private Integer intensity;
    @JsonProperty("carga")
    private Integer roadLoad;
    @JsonProperty("intensidadSat")
    private Integer saturationFlow;
    @JsonProperty("ocupacion")
    private Double occupancy;
    private Geometry location;
}